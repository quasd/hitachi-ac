# Lirc configuration files. 

- Hitachi RAS-AJ40G2 (AC) 
- Panasonic HK9487 (light) 
- iLife V5s Pro

## Install

Place the files in your /etc/lirc/lircd.conf.d/ and restart lircd

```
systemctl restart lircd
```

## How to Use

### iLife V5s Pro

```
isend SEND_ONCE VACUUM START_PAUSE
                       GO_HOME
                       MAX_POWER
                       CLEAN_SPOT
```

### Hitachi RAS-AJ40G2

```
irsend SEND_ONCE AC OFF
                    COOLING_25C
                    HEATING_20C
                    COOLING_24C_DIRECTION
```

### Panasonic HK9487

```
irsend SEND_ONCE panasonic-hk9487 pwr
                                  maxPwr
                                  nightMode
                                  pwrOff
                                  incBrightness
                                  decBrightness
                                  incColor
                                  decColor
                                  30minTimer
```
